const { toPromise, hex2buf, buf2hex, sleep, arrayBufferToString } = require('../utils/util')


const serviceId = '0000FFE0-0000-1000-8000-00805F9B34FB';
// const deviceId = '5C:31:3E:5E:48:7B';
const characteristicId = '0000FFE1-0000-1000-8000-00805F9B34FB';
// const characteristicId = 'FFE1';

class BleService {

  static async connect(deviceId) {
    await toPromise(wx.openBluetoothAdapter);
    return await toPromise(wx.createBLEConnection, { deviceId, });
  }

  static async disconnect(deviceId) {
    await this.writeToBle('5b455d', { deviceId, });
    const res = await toPromise(wx.closeBLEConnection, { deviceId, });
    console.log(res);

    await toPromise(wx.closeBluetoothAdapter);
  }

  static async writeToBle(value, { deviceId }) {
    const hexValue = hex2buf(value);

    console.log(buf2hex(hexValue.buffer));
    const notifyResult = await this._bleNotify(deviceId);
    console.log(notifyResult);
    
    console.log(hexValue, hexValue.buffer);
    

    return await toPromise(wx.writeBLECharacteristicValue, { deviceId, serviceId, characteristicId, value: hexValue.buffer, });
  }

  static async _bleNotify(deviceId) {
    await toPromise(wx.getBLEDeviceServices, { deviceId, });
    await toPromise(wx.getBLEDeviceCharacteristics, { deviceId, serviceId, });
    return await toPromise(wx.notifyBLECharacteristicValueChange, {
      state: true, // Enable the notify feature
      deviceId,
      serviceId,
      characteristicId,
    });
  }

  static async listenToBle() {
    let weight;
    wx.onBLECharacteristicValueChange(function(characteristic) {
      weight = arrayBufferToString(characteristic.value);
    })

    await sleep(7000);

    console.log('w in service: ', weight);
    return this._parseWeight(weight);
  }

  static _parseWeight(weight = '') {

    const splittedWeight = weight.split(',');
    console.log({splittedWeight});

    const lastSplitted = splittedWeight.pop();
    console.log({lastSplitted});

    const removeSpecialCharacter = lastSplitted.replace(/[\]]+/g, '');
    console.log({removeSpecialCharacter});

    return removeSpecialCharacter;
  }

}

module.exports = BleService;