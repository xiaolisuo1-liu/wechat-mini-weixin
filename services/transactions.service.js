const utils = require('../utils/util');
const app = new getApp();

async function getTransactionsList() {
    const url = `/activities/${app.globalData.userInfo.uid}?pageSize=20`;
    
    const result = await utils.ajax(url, null, 'GET', app.globalData.userInfo.token);

    if (result.code === "0000") {
        return result.data;
    }
    
}


module.exports = {
    getTransactionsList
};
