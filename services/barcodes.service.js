const app = new getApp();
const { ajax } = require('../utils/util');

class BarcodesService {
  static async redeemBarcode(barcode) {
    const URL = `/RP/redemptions/${app.globalData.user_profile.uid}`;

    const body = {
      barcode,
      item: "iCYCLE Barcode",
      rp: 60,
      value: "30",
      user: app.globalData.user_profile
    };

    const result = await ajax(URL, body, 'POST', app.globalData.userInfo.token);

    return result;
  }

}

module.exports = BarcodesService;