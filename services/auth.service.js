const { toPromise, ajax } = require('../utils/util');
const app = new getApp();

class AuthService {

  static async loginWithWechat() {
    
    const { code } = await toPromise(wx.login);
    
    const URL = `user/wechat-login`;
    
    const result = await ajax(URL, { js_code: code }, 'POST');

    if (result.code !== '0000') {
      throw new AuthError(result.message);
    }

    app.globalData.userInfo = result.data;
    wx.setStorageSync('userInfo', result.data);
    
    wx.redirectTo({ url: '../index/index', })
  }

}


class AuthError extends Error {
  constructor(message) {
    super(message);
  }
}


class UserNotFoundAuthError extends AuthError {
  constructor(message) {
    super(message);
  }
}


module.exports = { AuthService, AuthError, UserNotFoundAuthError };