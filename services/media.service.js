const env = require('../env');
const app = new getApp();

class MediaService {


  static async upload(filePath, type = 'Feedbacks') {
    if (!filePath) throw new Error('filePath is required!');

    return new Promise((resolve, reject) => {
      wx.uploadFile({
        url: `${env.apiUrl}/cloundinary`,
        filePath: filePath,
        header: {
          'content-type': "multipart/form-data",
          'client-key': 'CH!NA_@PP',
          'x-auth': app.globalData.userInfo.token,
          'languageId': env.languageId,
        },
        name: 'image',
        formData: { 'folder': type },
        success(res) {          
          const data = JSON.parse(res.data);
          if (data.code == '0000') {
            resolve(data.data);
          } else {
            reject(data.message);
          }
        },
        fail(err) {
          reject(err);
        }
      })

    });
  }
}

module.exports = MediaService;