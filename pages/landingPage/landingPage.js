const util = require('../../utils/util.js');
const { toPromise } = util;

// pages/landingPage/landingPage.js
Page({

  /**
   * Page initial data
   */
  data: {

  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

  onLogin: async function () {
    wx.navigateTo({
      url: '../login/login'
    })
  },

  onRegister: async function () {
    wx.showLoading({ title: '加载中' });

    try {
      const wxGetSettingResult = await toPromise(wx.getSetting);
      if (!wxGetSettingResult.authSetting['scope.userInfo']) {
        throw Error('Not Aouthorized to: scope.userInfo');
      }
      
      const [wxLoginResult, wxGetUserInfoResult] = await Promise.all([toPromise(wx.login), toPromise(wx.getUserInfo)]);
      
      if (!wxLoginResult.code) {
        throw Error('No login code');
      }

      const jsCode = wxLoginResult.code;
      const userInfo = JSON.stringify(wxGetUserInfoResult.userInfo);

      await toPromise(wx.navigateTo, { url: `../register/register?jsCode=${jsCode}&userInfo=${userInfo}` });
    
    } catch (error) {
      console.error(error);
      await toPromise(wx.navigateTo, { url: `../register/register` });

    } finally {
      wx.hideLoading();
    }
  },
})