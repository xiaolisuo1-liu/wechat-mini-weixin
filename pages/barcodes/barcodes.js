// pages/barcodes/barcodes.js
const app = new getApp();
const util = require('../../utils/util.js');
const barcodesService = require('../../services/barcodes.service');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '条码',
    user_profile: null,
    barcod:'',
    barcodes: [],
    barcodesToAdd: [],
    barcodeInput: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  
  onLoad: function(options) {
    this.reloadProfile();
    const data = app.globalData.user_profile;
    data.caozuozt = false;
    this.setData({
      user_profile: data,
      barcodes: data.barcodes,
    })

    console.log(this.data.barcodes);
  },
  signout() {
    wx.setStorageSync('userInfo', {});
    app.globalData.userInfo = null;
    wx.redirectTo({
      url: '../landingPage/landingPage',
    })
  },
  zhankaicd() {
    this.setData({
      ['user_profile.caozuozt']: !this.data.user_profile.caozuozt
    })
  },
  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.redirectTo({
      url: url,
    })
  },
  
  iDrop() {
    wx.scanCode({
      success(res) {
        this.tjbarcode(res.data)
      }
    })
  },
  tjbarcode(){
    util.ajax('barcodes/' + app.globalData.user_profile.uid, { barcodes: [this.data.barcod]}, 'POST', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        util.showToast(res.data.message, 'none')
        
        util.ajax('user/profile', {}, 'GET', app.globalData.userInfo.token).then(res => {
          if (res.code == '0000') {
            app.globalData.user_profile = res.data;
            this.setData({
              user_profile: res.data,
            })
          }
        }, err => {
          util.showToast(err.errMsg, 'none')
        })
      }else{
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  shuru(e){
    this.setData({
      barcod: e.detail.value
    })
  },
  tzditu() {
    this.setData({
      ['user_profile.xsditu']: !this.data.user_profile.xsditu
    })
  },
  tzhelp() {
    this.setData({
      ['user_profile.xshelp']: !this.data.user_profile.xshelp
    })
  },
  addBarcode(barcode) {
    const barcodes = this.data.barcodesToAdd;
    barcodes.push(barcode);
    this.setData({
      barcodesToAdd: barcodes
    });
  },
  onAddBarcode(input) {
    const  newBarcode = input.detail.value ?? this.data.barcodeInput;

    if (!newBarcode || newBarcode === '') {
      return;
    }
    this.addBarcode(newBarcode);
  },
  onRemoveBarcode(target) {
    const index = target.currentTarget.id;
    const barcodes = this.data.barcodesToAdd;
    barcodes.splice(index, 1);
    this.setData({
      barcodesToAdd: barcodes,
    });
  },
  onScan() {
    const _this = this;
    wx.scanCode({
      success(res) {
        const barcode = res.result;
        _this.addBarcode(barcode);
      }
    })
  },
  bindBarcodeInput(e) {
    this.setData({
      barcodeInput: e.detail.value,
    });
  },
  onSubmit() {
    if (this.data.barcodesToAdd.length < 1) {
      return;
    }
    util.ajax('barcodes/' + app.globalData.user_profile.uid, { barcodes: this.data.barcodesToAdd }, 'POST', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        util.showToast(res.data.message, 'none')

        this.setData({ barcodesToAdd: [] });

        const _this = this;
        setTimeout(function() {
          _this.onLoad();
        }, 1550)
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  reloadProfile() {
    util.ajax('user/profile', {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        app.globalData.user_profile = res.data;
        this.setData({
          user_profile: res.data,
          barcodes: res.data.barcodes,
        })
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  onBarcodeSelected(event) {
    const barcode = this.data.barcodes[event.target.id];

    if (!barcode) {
      console.log('No Barcode');
      return;
    }

    wx.showModal({
      cancelColor: 'cancelColor',
      title: '确认条形码兑换',
      content: `兑换 ${barcode}?`,
      success: async (res) => {
        if (res.cancel) {
          return;
        }

        const apiResult = await barcodesService.redeemBarcode(barcode);
        if (apiResult.code == "0000") {
          wx.showToast({
            title: 'Redeemed Successfully',
          })
        }
      }
    })
    
  },
})