const utils = require('../../utils/util.js');
const { toPromise, throttle } = require('../../utils/util.js');

const app = new getApp();


// pages/map/map.js
Page({

  /**
   * Page initial data
   */
  data: { 
    longitude: undefined,
    latitude: undefined,
    markers: [],
    mapContext: {},
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    wx.getLocation({
      success: res => {
        this.setData({ latitude: res.latitude, longitude: res.longitude })
        this.getBins(this.data.longitude, this.data.latitude)
          .then(res => this.setMarkers(res.data))
      },
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: async function() {
    this.setData({
      mapContext: wx.createMapContext('map'),
    });
  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {

  },

  getBins: (longitude, latitude) => {
    const gps = { lat: latitude, lng: longitude };
    return utils.ajax('bin/nearby', { gps }, 'POST', app.globalData.userInfo.token);
    // ------------------------------------------------
  },

  setMarkers: function(bins = []) {
    const markers = bins.map(bin => {
      return {
        title: bin.bin_name,
        iconPath: "/images/map-pin.png",
        id: bin.bin_Id,
        latitude: bin.gps.lat,
        longitude: bin.gps.lng,
        width: 50,
        height: 50
      }
    });
    
    this.setData({ markers: [...this.data.markers, ...markers] });
  },

  onViewChange: async function(params) {
    if (params.type !== 'end') {
      return;
    }
    
    throttle(() => {
      this.data.mapContext.getCenterLocation({
        success: (data) => {
          this.getBins(data.longitude, data.latitude)
          .then(res => this.setMarkers(res.data))
        },
        fail: console.error
      });
    }, 4000);
    

  },
})