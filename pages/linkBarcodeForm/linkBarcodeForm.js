const util = require('../../utils/util');
const app = getApp();
// pages/linkBarcodeForm/linkBarcodeForm.js
Page({

  /**
   * Page initial data
   */
  data: {
    barcode: null,
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      barcode: options.barcode,
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {
  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

  formSubmit: async function (event) {
    const formData = event.detail.value;

    if (!this.formValidation(formData)) {
      return;
    }

    // call api
    try {
      const result = await util.ajax('/barcodes/update/address',
        formData,
        'PUT',
        app.globalData.userInfo.token,
      );

      wx.reLaunch({url: '../index/index'});
    } catch (error) {
      console.error(error);
    }


  },

  formValidation: function (formData) {
    if (!util.isset(formData.email)) {
      util.showToast('Email is required!', 'none');
      return false;
    }
    if (!util.isset(formData.address)) {
      util.showToast('Address is required!', 'none');
      return false;
    }
    if (!util.isset(formData.country)) {
      util.showToast('Country is required!', 'none');
      return false;
    }
    if (!util.isset(formData.barcode)) {
      util.showToast('barcode is required!', 'none');
      return false;
    }

    return true;
  }
})