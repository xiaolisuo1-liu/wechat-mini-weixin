// pages/details/details.js
const app = new getApp();
const util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bird:null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.getshuju(options.id)
  },
  getshuju(id){
    util.ajax('birds/'+id, {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        this.setData({
          bird: res.data
        })
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  }
})