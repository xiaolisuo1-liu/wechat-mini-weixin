// pages/otp/otp.js
const utils = require('../../utils/util.js');
Page({

  /**
   * Page initial data
   */
  data: {
    digits: [
      { value: null, focus: true },
      { value: null, focus: false },
      { value: null, focus: false },
      { value: null, focus: false },
      { value: null, focus: false },
      { value: null, focus: false },
    ]
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

  onDigitInput: function (e) {
    const digitIndex = e.currentTarget.dataset.index;
    
    this.setValue(digitIndex, e.detail.value);

    if (!e.detail.value) {
      this.setFocus(digitIndex - 1, true);
      return;
    }

    if (this.isLastDigitIndex(digitIndex)) {
      return this.sumbit();
    }

    if (digitIndex >= this.data.digits.length - 1) {
      return;
    }

    this.setFocus(digitIndex + 1, true);
  },

  setFocus: function (index, status = false) {
    if (this.data.digits[index] === undefined) {
      return;
    }
    const newArray = this.data.digits;
    newArray[index].focus = status;
    this.setData({
      digits: newArray
    });
  },

  setValue: function (index, value) {
    const newArray = this.data.digits;
    newArray[index].value = value;
    this.setData({
      digits: newArray
    })
  },

  setIsValid: function (index, status = false) {
    if (this.data.digits[index] === undefined) {
      return;
    }

    const newArray = this.data.digits;
    newArray[index].isValid = status;
    this.setData({
      digits: newArray
    })
  },

  onDigitBlur: function (e) {
    const digitIndex = e.currentTarget.dataset.index;
    this.setFocus(digitIndex, false);
  },

  sumbit: function () {
    if (!this.isDigitsArrayValid()) {
      return;
    }
    console.log(this.getFullDigit())
    const url = `otp/verify/${this.getFullDigit()}`;
    console.log(url);
    utils.ajax(url, null, 'GET')
      .then(res => {
        if (res.code !== "0000") {
          utils.showToast('成功');
          return wx.reLaunch({
            url: '/pages/forgotPassword/forgotPassword',
          })
        }

        utils.showToast('Failed, please try again', 'none');
        wx.reLaunch({
          url: `/pages/resetPassword/resetPassword?otp=${this.getFullDigit()}`,
        })
      })
      .catch(console.error)
  },

  isDigitsArrayValid: function () {
    return !this.data.digits.some(digit => !utils.isset(digit.value));
  },

  isLastDigitIndex: function (index) {
    return index === this.data.digits.length - 1;
  },

  getFullDigit: function () {
    const mapped = this.data.digits.map(digit => String(digit.value));
    return mapped.join('');
  }
})