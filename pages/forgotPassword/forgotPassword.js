const app = new getApp();
const util = require("../../utils/util.js");

// pages/forgotPassword/forgotPassword.js
Page({

  /**
   * Page initial data
   */
  data: {
    email_phone: null,
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {

  },

  binEmailPhoneInput: function(e) {
    this.setData({
      email_phone: e.detail.value
    });
  },

  formSubmit: function() {
    const _this = this;
    wx.showLoading({ title: '加载中' })
    wx.requestSubscribeMessage({
      tmplIds: ['NT_XbITNrauKdTWZL_xcMWmiQ_it-jMzwwBbkkTsY_s'],
      success(res) {
        const body = { email_phone: _this.data.email_phone };
        util.ajax('forgotPassword', body, 'POST')
          .then(wx.redirectTo({
            url: '/pages/otp/otp',
          }))
          .catch(console.error)
      },
      complete: () => wx.hideLoading()
    })
  },

  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
})