const transactionsService = require('../../services/transactions.service');
const { parseDate } = require('../../utils/parseDate.util');
const { stringTrimPipe } = require('../../utils/stringTrim.util');


// pages/transactionsList/transactionsList.js
Page({

  /**
   * Page initial data
   */
  data: {
    transactions: [],
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: async function (options) {
    let { data } = await transactionsService.getTransactionsList();
    stringTrimPipe(data, 15, ['bin_name']);
    parseDate(data);
    console.log(data);
    
    this.setData({
      transactions: data,
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})