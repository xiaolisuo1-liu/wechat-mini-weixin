const app = new getApp();
const env = require('../../env');

// pages/h5.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    xsditu: false,
    xshelp: false,
    xswjmm:false,
    guidance:false,
    pointSummary: false,
    pointSummaryUrl: null,
    userProfile: app.globalData.userInfo,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.type == 'ditu') {
      this.setData({
        xsditu: true
      })
    }
    if (options.type == 'help') {
      this.setData({
        xshelp: true
      })
    }
    if (options.type == 'wjmm') {
      this.setData({
        xswjmm: true
      })
    }
    if (options.type == 'guidance') {
      this.setData({
        guidance: true
      })
    }

    if (options.type == 'pointSummary') {
      this.setData({
        pointSummary: true,
        pointSummaryUrl: `${env.webUrl}profile/summary_points?cookie=${this.data.userProfile.token}`
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})