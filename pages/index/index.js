//index.js
//获取应用实例
const app = new getApp();
const util = require('../../utils/util.js');
const { toPromise, ab2str } = require('../../utils/util.js');
const BleService = require('../../services/ble_service.js');

Page({
  data: {
    zst: false,
    liebiaoindex: 0,
    banners: [],
    news: [],
    user_profile: app.globalData.user_profile,
    zfxz: 0,
    zfzs: false,
    phones: 0,
    directbtn: 0,
    timex: ''
  },
  onLoad: function() { 
    // var demo = wx.getStorageSync('userInfo')
    // console.log(demo);
    this.setData({
      zst: true
    })
    setTimeout(() => {
      this.setData({
        zst: false
      })
    }, 2000)
    this.profile();
    this.getBanners();
    this.events();
  
    wx.getLocation({
      isHighAccuracy: true,
      success: (res)=>{
        // console.log(res, '定位后的信息');
        //中国经度范围:73°33′E至135°05′E;纬度范围:3°51′N至53°33′N
        if(res.latitude < 3.51 && res.latitude > 53.33 && res.longitude < 73.33 && res.longitude > 135.05){
          this.setData({
            phones: 1
          })
        }else{
          this.setData({
            phones: 0
          })
        }
        
       
      },
      fail: (err)=>{
        console.log(err);
      }
    })
  
  
  },
  onPullDownRefresh: function() {
    wx.stopPullDownRefresh()
    if (this.data.user_profile && this.data.user_profile.caozuozt) return;
    this.setData({
      zst: true
    })
    setTimeout(() => {
      this.setData({
        zst: false
      })
    }, 2000)
    this.profile();
    this.getBanners();
    this.events();

  },
  signout() {
    wx.setStorageSync('userInfo', {});
    app.globalData.userInfo = null;
    wx.redirectTo({
      url: '../landingPage/landingPage',
    })
  },
  zfxz(e) {
    this.setData({
      zfxz: e.currentTarget.dataset.index
    })
  },
  gbtx() {
    this.setData({
      zfzs: !this.data.zfzs
    })
  },
  async redeemReward(){
    try{
      const res = await util.ajax('wechat-newuser/' + app.globalData.userInfo.uid, {}, 'GET', app.globalData.userInfo.token);
      console.log(res);

      if(res.code !== '0000'){
        throw new Error(res.message)
      }

      app.globalData.user_profile.isRewarded = true
      this.setData({
        user_profile: app.globalData.user_profile
      })
      
      await wx.redirectTo({
        url: '../h5/h5?type=guidance'
      })

    } catch (error) {
      console.log(error);
      await util.showToast(error.message || error.errMsg || '错误码', 'none');
    }
  },
  onShow() {

  },
  async iDrop() {
    try {

      const location = await toPromise(wx.getLocation, { type: 'wgs84' });
      const coordinates = [location.longitude, location.latitude];    
      
      const scanResult = await toPromise(wx.scanCode);

      let bin_id;
      let barcode;
      let bleDeviceId;
      let weight;
      if (util.codeStructureIsDefined(scanResult.result)) {
        const decoded = util.base64_decode(scanResult.result);
        console.log(decoded);
        bin_id = JSON.parse(decoded).content.binCode;
        bleDeviceId = JSON.parse(decoded).content.bluetoothId;
      } else {
        barcode = scanResult.result;
      }

      if (bleDeviceId) {
        try {
          await toPromise(wx.showLoading, { title: 'Connecting...', mask: true, });
          const connResult = await BleService.connect(bleDeviceId);
          console.log('Connection Result: ', connResult);
          
          const writeResult = await BleService.writeToBle('5b535d', { deviceId: bleDeviceId, });
          console.log('Write Result: ', writeResult);
          
          await toPromise(wx.showLoading, { title: 'Weighting...', mask: true, });
          
          weight = await BleService.listenToBle();

          console.log(weight);
        } catch (error) {
          console.log(error);
          await BleService.disconnect(bleDeviceId).catch(console.error)
        }
        wx.hideLoading();

      }
      
      const body = {
        coordinates,
        bin_id,
        barcode,
        data: {
          weight,
        },
      };  

      const res = await util.ajax('activities/idrop/' + app.globalData.userInfo.uid, body, 'POST', app.globalData.userInfo.token);
      console.log(res);

      if (res.code == '9963') {
        const url = `../linkBarcodeForm/linkBarcodeForm?barcode=${barcode}`;
        await util.toPromise(wx.navigateTo, { url, });
        return;
      }

      if (res.code !== '0000') {
        throw new Error(res.message);
      }

      if (res.data.action === 'feedback') {
        const url = `../feedback/feedback?uid=${res.data.barcodeUser.uid}&barcode=${res.data.barcodeUser.barcode}&feedbackType="barcode"`;
        await util.toPromise(wx.navigateTo, { url, });
        return;
      }
      
      if (weight) {
        await toPromise(wx.showModal, { showCancel: false, title: 'KG', content: weight || '' });
      }

      util.showToast(res.data.message, 'none')
    } catch (error) {
      console.log(error);
      await util.showToast(error.message || error.errMsg || '错误码', 'none');
    }
  },
  iPickupScan() {
    wx.getLocation({
      type: 'wgs84',
      success(location) {
        const coordinates = [location.longitude, location.latitude];          
        wx.scanCode({
          success(res) {
            console.log(res.result);  //跳转
            wx.setStorageSync('datacode', res.result);
            wx.navigateTo({
              url: '../feedback/feedback',
            })
            let bin_id;
            let barcode;
            if (util.codeStructureIsDefined(res.result)) {
              const decoded = util.base64_decode(res.result);
              bin_id = JSON.parse(decoded).content.binCode;
            } else {
              barcode = res.result;
            }

            const body = {
              coordinates,
            };

            if (bin_id) {
              body.bin_id = bin_id;
            }

            if (barcode) {
              body.barcode = barcode;
            }

            console.log(body);
            util.ajax('activities/ipickup/' + app.globalData.user_profile.uid, body, 'POST', app.globalData.userInfo.token).then( async res => {
              console.log(res);
              if (res.code == '9963') {
                const url = `../linkBarcodeForm/linkBarcodeForm?barcode=${barcode}`;
                await util.toPromise(wx.navigateTo, { url, });
                return;
              }

              if (res.code == '0000') {
                util.showToast(res.data.message, 'none')
              } else {
                util.showToast(res.message, 'none')
              }
            }, err => {
              util.showToast(err.errMsg, 'none')
            })
          },
          fail(err) {
            console.log(err)
            util.showToast('错误码', 'none')
          }
        })

      }
    })
  },
  iDrop2() {
        if(this.data.directbtn == 0){
          wx.getLocation({
            success(location) {
              const coordinates = [location.longitude, location.latitude];
              util.ajax('activities/idrop/' + app.globalData.user_profile.uid, {
                coordinates
              }, 'POST', app.globalData.userInfo.token)
              .then(res => {
                if (res.code == '0000') {
                  util.showToast(res.data.message, 'none')
                } else {
                  util.showToast(res.message, 'none')
                }
              }, err => {
                util.showToast(err.errMsg, 'none')
              })
            }
}) 
          this.setData({
            timex: Date.parse(new Date()) + 60000,
            directbtn: 1
          })
          return;
        }
        if(this.data.directbtn == 1){
          if(Date.parse(new Date()) >= this.data.timex){
            this.setData({
              directbtn: 0
            })
          }else{
                 wx.showToast({
                  title: '已投递,请在一分钟后再次投递',
                  icon: 'none'
                })
          }
          return
        }


      this.setData({
        timex: Date.parse(new Date())
      })
      
  //  console.log(this.data.directbtn);
  //  if(this.data.directbtn == 0){
  
  //    this.setData({
  //      directbtn: 1
  //    })
  //  }else{
  //   wx.showToast({
  //     title: '已投递,请在一分钟后再次投递',
  //     icon: 'none'
  //   })
  //   clearInterval(timers);
  //    var timers = setTimeout(() => {
  //      console.log('开始投递');
  //     this.setData({
  //       directbtn: 0,
  //     })
  //    }, 60000);
  //  }
  },
  iPickup() {
    console.log('IPICKUP');
    wx.scanCode({
      success(res) {
        const body = {};
        if (util.codeStructureIsDefined(res.result)) {
          const decoded = util.base64_decode(res.result);
          body.bin_id = JSON.parse(decoded).content.binCode;
        } else {
          body.bin_id = res.result;
          body.barcode = res.result;
        }
        util.ajax('activities/ipickup/' + app.globalData.user_profile.uid, body, 'POST', app.globalData.userInfo.token).then(res => {
          console.log(res);
          
          if (res.code == '0000') {
            util.showToast(res.data.message)
          } else {
            util.showToast(res.message, 'none')
          }
        }, err => {
          util.showToast(err.errMsg, 'none')
        })
      }
    })
  },
  iRedeem(e) {
    const value = e.detail.value;
    if (value.rp == '' || value.phone == '' || value.acc == '' || this.data.zfxz == 0) {
      util.showToast('请填写完整信息。', 'none');
      return;
    }
    const data = {}
    data.rp = value.rp;
    data.payment = {};
    data.payment.account_name = app.globalData.user_profile.username;
    data.payment.account_phone = value.phone;
    data.payment.account_number = value.acc;
    data.payment.payment_method = this.data.zfxz == 2 ? 'alipay' : "wechatpay";

    util.ajax('RP/redemptions/' + app.globalData.user_profile.uid, data, 'POST', app.globalData.userInfo.token).then(res => {      
      console.log(res);
      
      if (res.code == '0000') {
        util.showToast('我们将在x天内处理您的申请，如果您在x天后尚未在支付宝/微信钱包中收到您的兑换数额，请联系我们。', 'success')
        this.setData({
          zfzs: false
        })
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  getBanners() {
    util.ajax('banners', {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        const data = res.data;
        data.caozuozt = false
        this.setData({
          banners: data.banners,
        })
      }
    }, err => {
      util.showToast('加载失败，下拉导航刷新', 'none')
    })
  },
  events() {
    util.ajax('events', {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        const data = res.data;
        data.caozuozt = false
        this.setData({
          news: data.events,
        })
      }
    }, err => {
      util.showToast('加载失败，下拉导航刷新', 'none')
    })
  },
  profile() {
    this.setData({
      ['user_profile.caozuozt']: false,
    })
    util.ajax('user/profile', {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        app.globalData.user_profile = res.data;
        const data = res.data;
        data.caozuozt = false
        this.setData({
          user_profile: data,
        })
      }
    }, err => {
      util.showToast('加载失败，下拉导航刷新', 'none')
    })
  },
  qiehuan(e) {
    const qhindex = e.currentTarget.dataset.index;
    if (qhindex == this.liebiaoindex) return;
    this.setData({
      liebiaoindex: qhindex
    })
  },
  zhankaicd() {
    this.setData({
      ['user_profile.caozuozt']: !this.data.user_profile.caozuozt
    })
  },
  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
  tzditu() {
    this.setData({
      ['user_profile.xsditu']: !this.data.user_profile.xsditu
    })
  },
  tzhelp() {
    this.setData({
      ['user_profile.xshelp']: !this.data.user_profile.xshelp
    })
  }
})