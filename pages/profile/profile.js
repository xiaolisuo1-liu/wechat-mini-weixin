const app = new getApp();
const util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '个人资料',
    user_profile: null,
    xz: true,
    user: {
      username: '',
      family: '',
      email: '',
      phone: '',
      avatar: '',
      gender: '',
      location: {
        address: '', state: "", postcode: ""
      }
    }
  },
  labdzp() {
    var _this = this;
    wx.chooseImage({
      count: 1, // 默认9 
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有 
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有 
      success: function(res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片 
        _this.setData({
          ['user_profile.avatar']: res.tempFilePaths[0],
          ['user.avatar']: res.tempFilePaths[0]
        })



      }
    })
  },
  onLoad: function(options) {
    const data = app.globalData.user_profile;
    data.caozuozt = false;
    console.log(data)
    this.setData({
      user_profile: data,
      xz: data.user_type,
      ['user.username']: data.username,
      ['user.phone']: data.phone,
      ['user.email']: data.email,
      ['user.uid']: app.globalData.userInfo.uid,
      ['user.avatar']: data.avatar
    })
  },
  zhankaicd() {
    this.setData({
      ['user_profile.caozuozt']: !this.data.user_profile.caozuozt,
    })
  },
  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.redirectTo({
      url: url,
    })
  },
  qihuan(e) {
    console.log(e)
    if (e.currentTarget.dataset.type == 1) {
      this.setData({
        xz: true
      })
    } else {
      this.setData({
        xz: false
      })
    }
  },
  getshuju(e) {
    console.log(e)
    const type = e.currentTarget.dataset.type;
    const value = e.detail.value;
    if (type == 'name') {
      this.setData({
        ['user.username']: value + this.data.user.family
      })
    } else if (type == 'family') {
      this.setData({
        ['user.username']: this.data.user.username + value
      })
    } else if (type == 'email') {
      this.setData({
        ['user.email']: value
      })
    } else if (type == 'phone') {
      this.setData({
        ['user.phone']: value
      })
    } else if (type == 'address') {
      this.setData({
        ['user.location.address']: value
      })
    }
  },
  save() {
    const that = this;
    util.ajax('users/' + app.globalData.userInfo.uid, this.data.user, 'PUT', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        util.showToast('success', 'none');
        util.ajax('user/profile', {}, 'GET', app.globalData.userInfo.token).then(res => {
          if (res.code == '0000') {
            app.globalData.user_profile = res.data;
          }
        }, err => {
          util.showToast(err.errMsg, 'none')
        })
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    });
    console.log(that.data.user)
    wx.uploadFile({
      url: 'https://icycle-api-dev.herokuapp.com/api/v1.0/cloundinary',
      filePath: that.data.user.avatar,
      header: {
        'content-type': "multipart/form-data",
        'client-key': 'CH!NA_@PP',
        'x-auth': app.globalData.userInfo.token,
        'languageId': 3
      },
      name: 'file',
      formData: that.data.user,
      success(res) {
        if (res.data.code == '0000') {
          util.showToast(res.data.message, 'none')
          app.globalData.user_profile.avatar = res.data.data
        } else {
          util.showToast(res.data.message, 'none')
        }
      }
    })
  },
  tzditu() {
    this.setData({
      ['user_profile.xsditu']: !this.data.user_profile.xsditu
    })
  },
  tzhelp() {
    this.setData({
      ['user_profile.xshelp']: !this.data.user_profile.xshelp
    })
  }
})