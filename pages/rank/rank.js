// pages/rank/rank.js
const app = new getApp();
const util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ranks: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function(options) {

    const locationData = await util.toPromise(wx.getLocation, {type: 'wgs84'});

    const body = {
      "coordinates" : [ 
        locationData.longitude, 
        locationData.latitude,
      ]
    };
    

    util.ajax('birds/leaderboard', body, 'POST', app.globalData.userInfo.token).then(res => {
      console.log(res.data);
      if (res.code == '0000') {
        const rank = res.data.data;
        rank.forEach(item => {
          const r = Math.floor(Math.random() * 255);
          const g = Math.floor(Math.random() * 255);
          const b = Math.floor(Math.random() * 255);
          item.color = `rgb(${r},${g},${b})`;
          item.szm = item.username[0]
        })
        this.setData({
          ranks: rank
        })
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    });
  },
  xihuan(e) {
    const rank = this.data.ranks
    const index = e.currentTarget.dataset.index
    if (rank[index].xh) {
      rank[index].xh = false;
      rank[index].likes -= 1

    } else {
      rank[index].xh = true;
      rank[index].likes += 1
    }

    this.setData({
      ranks: rank
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})