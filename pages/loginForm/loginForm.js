const util = require('../../utils/util.js');
const { toPromise } = util;
const { AuthService, AuthError, UserNotFoundAuthError } = require('../../services/auth.service');
const app = new getApp();

// pages/loginForm/loginForm.js
Page({

  /**
   * Page initial data
   */
  data: {
    butfil: true,
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

  formSubmit: async function(e) {
    if (!this.data.butfil) return;
    const value = e.detail.value;
    if (value.email == '' || value.password == '') {
      util.showToast('Email/Phone is not valid', 'none');
      return;
    }
    this.setData({
      butfil: false
    })
    const { code } = await toPromise(wx.login);
    util.ajax('user/signin', {
      email_phone: value.email,
      password: value.password,
      js_code: code,
    }).then(res => {
      if (res.code == "0000") {
        
        app.globalData.userInfo = res.data;
        wx.setStorageSync('userInfo', res.data);

        wx.redirectTo({
          url: '../index/index',
        })
      } else {
        util.showToast(res.message, 'none');
      }
      this.setData({
        butfil: true
      })
    }, err => {
      util.showToast(err.errMsg, 'none');
      this.setData({
        butfil: true
      })
    })
  },
})