// pages/birds/birds.js
const app = new getApp();
const util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '寻禽记',
    user_profile: null,
    birds: {
      birds:[]
    },
  },
  signout() {
    wx.setStorageSync('userInfo', {});
    app.globalData.userInfo = null;
    wx.redirectTo({
      url: '../landingPage/landingPage',
    })
  },
  birtiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url + '?id=' + e.currentTarget.dataset.birdsid,
    })
  },
  getbirdsLis() {
   
    util.ajax('birds', {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        // this.setData({
        //   birds: res.data
        // })
        util.ajax('users/birds/' + app.globalData.userInfo.uid, {}, 'GET', app.globalData.userInfo.token).then(res => {
          console.log(res)
          if (res.code == '0000') {
            this.setData({
              ["birds.birds"]: this.data.birds.birds.concat(res.data)
            })
          } else {
            //util.showToast(res.message, 'none')
          }
        }, err => {
          //util.showToast(err.errMsg, 'none')
        })
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  onLoad: function(options) {
    const data = app.globalData.user_profile;
    data.caozuozt = false;
    this.setData({
      user_profile: data
    })
    this.getbirdsLis();
  },
  zhankaicd() {
    this.setData({
      ['user_profile.caozuozt']: !this.data.user_profile.caozuozt
    })
  },
  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.redirectTo({
      url: url,
    })
  },
  tzditu() {
    this.setData({
      ['user_profile.xsditu']: !this.data.user_profile.xsditu
    })
  },
  tzhelp() {
    this.setData({
      ['user_profile.xshelp']: !this.data.user_profile.xshelp
    })
  }

})