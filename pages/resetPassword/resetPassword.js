// pages/resetPassword/resetPassword.js
const utils = require('../../utils/util.js');
Page({

  /**
   * Page initial data
   */
  data: {
    otp: null,
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({ otp: options.otp });
    console.log('OTP: ', this.data.otp)
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

  formSubmit: function (event) {
    const formData = event.detail.value;
    if (formData.password !== formData.confirmPassword) {
      utils.showToast('Password not equel confirm password!', 'none');
      return;
    }
    if (formData.password.length < 8) {
      utils.showToast('必须设置一个8位的密码', 'none');
      return;
    }

    const url = '/resetPassword-otp';  
    const body = { otp: this.data.otp, password: formData.password, confirm_password: formData.confirmPassword };
    utils.ajax(url, body, 'POST')
      .then(res => {
        console.log(res);
        if (res.code !== '0000') {
          utils.showToast(res.message, 'none');
          return;
        }

        utils.showToast('Successful');
        return wx.reLaunch({
          url: '/pages/login/login',
        })
      })
      .catch()
  }
})