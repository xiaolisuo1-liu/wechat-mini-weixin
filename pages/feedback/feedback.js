// pages/feedback/feedback.js
// pages/birds/birds.js
const app = new getApp();
const util = require('../../utils/util.js');
const toPromise = util.toPromise;
const CameraService = require('../../services/camera.service');
const MediaService = require('../../services/media.service');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'意见反馈',
    user_profile: null,
    birds: '',
    bq:0,
    huayu:'',
    feedbackType: null,
    reportedUid: null,
    reportedUserBarcode: null,
    imageTempPath: null,
  },
  async tijiao(){
    try {

      if (!this.data.huayu || this.data.huayu == '') {
        wx.showToast({
          title: 'Comment is required!',  //评论是必须的
          icon: 'none',
        })
        return;
      }

      let imageUploadResult;
      let imageUrl;
      if (this.data.imageTempPath) {
        wx.showLoading({
          title: '加载中',
        })
        imageUploadResult = await MediaService.upload(this.data.imageTempPath); 
        
        if (!imageUploadResult || !imageUploadResult.length || !imageUploadResult[0].url) {
          throw new Error('Image not uploaded!');
        }
        
        imageUrl = imageUploadResult[0].url;
      }

      const data = {
        phone: app.globalData.userInfo.phone,
        rating: this.data.bq,
        comment:this.data.huayu,
        feedback_type: this.data.feedbackType,
        user_type: app.globalData.userInfo.user_type,
        image_url: imageUrl,
        extra: {
          uid: this.data.reportedUid,
          barcode: this.data.reportedUserBarcode,
        },
      }
      util.ajax('feedbacks', data , 'POST', app.globalData.userInfo.token).then( async res => {
        if (res.code == '0000') {
          wx.showToast({ title: res.data.message, icon: 'success' });
          wx.redirectTo({ url: '../index/index', })
        } else {
          util.showToast(res.message, 'none')
        }
      }, err => {
        util.showToast(err.errMsg, 'none')
      })
    } catch (error) {
      console.log(error);
    }

    wx.hideLoading()

    
  },

  shuru(e){
    this.setData({
      huayu: e.detail.value
    })
  },
  signout() {
    wx.setStorageSync('userInfo', {});
    app.globalData.userInfo = null;
    wx.redirectTo({
      url: '../landingPage/landingPage',
    })
  },
  qiehuan(e){

    this.setData({
      bq: e.currentTarget.dataset.index
    })
  },
  birtiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url + '?id=' + e.currentTarget.dataset.birdsid,
    })
  },
  onLoad: function (options) {
    // console.log(1);
    var values = wx.getStorageSync('datacode');
    const data = app.globalData.user_profile;
    data.caozuozt = false;
    console.log(values);
    this.setData({
      user_profile: data,
      feedbackType: options.feedbackType,
      reportedUid: options.uid,
      reportedUserBarcode: values,    ///-----------------------
    })
  },

  onShow() {
    this.setData({ imageTempPath: CameraService.imageTempPath })
  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {
    CameraService.imageTempPath = null;
  },

  zhankaicd() {
    this.setData({
      ['user_profile.caozuozt']: !this.data.user_profile.caozuozt
    })
  },
  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.redirectTo({
      url: url,
    })
  },
  tzditu() {
    this.setData({
      ['user_profile.xsditu']: !this.data.user_profile.xsditu
    })
  },
  tzhelp() {
    this.setData({
      ['user_profile.xshelp']: !this.data.user_profile.xshelp
    })
  },

  async takePhoto() {

    await toPromise(wx.navigateTo, { url: '../camera/camera' });
    
  },
  onCameraError(error) {
    console.error(error);
  },
})