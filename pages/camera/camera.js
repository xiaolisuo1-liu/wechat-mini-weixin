const CameraService = require('../../services/camera.service');
const { toPromise } = require('../../utils/util')

// pages/camera/camera.js
Page({

  /**
   * Page initial data
   */
  data: {
    imageTempPath: null,
  },

  onReady() {
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.camera']) {
          wx.authorize({
            scope: 'scope.camera',
            success () {
              console.log('sucess');
            }
          })
        }
      }
    })
  },

  onShutter() {
    const camera = wx.createCameraContext();
    
    camera.takePhoto({ 
      quality: 'low',
      success: data => {
        console.log(data.tempImagePath);
        
        this.setData({ imageTempPath: data })
      },
      fail: console.error,
      complete: () => console.log('Camera completed'),
    });
  },

  onComfirm() {
    CameraService.imageTempPath = this.data.imageTempPath.tempImagePath;

    wx.navigateBack({
      delta: 1,
    })
  },

  onCancel() {
    this.setData({ imageTempPath: null })
  }
})