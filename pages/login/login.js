// pages/login/login.js
const util = require('../../utils/util.js');
const { toPromise } = util;
const { AuthService, AuthError, UserNotFoundAuthError } = require('../../services/auth.service');
const app = new getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    butfil: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },
  tiaozhuan(e) {
    const url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
  onShareAppMessage: function() {

  },

  async onLoginWithWechat() {
    try {
      await AuthService.loginWithWechat();

      util.showToast('');
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundAuthError)
        return await toPromise(wx.navigateTo, { url: `../register/register` });

      if (error instanceof AuthError) {
        util.showToast(error.message, 'none');
      } else {
        util.showToast('执行此项操作时出错，请重试。', 'none');      
      }
    }
  },
  
  async onLoginForm() {
    wx.navigateTo({
      url: '../loginForm/loginForm',
    })
  },
})