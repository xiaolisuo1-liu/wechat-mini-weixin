// pages/details/details.js
const app = new getApp();
const util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    events:null,
  },
  onLoad: function (options) {
    console.log(options)
    if (options.type == "event"){
      this.getshujuevent(options.id)
    } else if (options.type == "banner"){
      this.getshujubanner(options.id)
    }
    
  },
  getshujubanner(id){
    util.ajax('banners/' + id, {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        const data = res.data;
        data.end_date = this.formatDate(new Date(data.publish_expiry_date));
        data.start_date = this.formatDate(new Date(data.publish_date));
        this.setData({
          events: data
        })
        console.log(this.data.events)
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  getshujuevent(id) {
    util.ajax('events/' + id, {}, 'GET', app.globalData.userInfo.token).then(res => {
      if (res.code == '0000') {
        const data = res.data;
        data.end_date = this.formatDate(new Date(data.end_date));
        data.start_date = this.formatDate(new Date(data.start_date));
        this.setData({
          events: data
        })
        console.log(this.data.events)
      } else {
        util.showToast(res.message, 'none')
      }
    }, err => {
      util.showToast(err.errMsg, 'none')
    })
  },
  formatDate(now) {
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var date = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    return year + "." + month + "." + date + " " + hour + ":" + minute + ":" + second;
  } 
})