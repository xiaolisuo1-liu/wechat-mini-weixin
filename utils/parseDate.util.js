const moment = require('moment');

async function parseDate(data) {
    try {
        if (Array.isArray(data)) {
            return _parseDateInArray(data);
        }

        if (typeof data === 'object' && data !== null && !(data instanceof Date)) {
            return _parseDateInObject(data);
        }

        return _parseDateInStringOrDateObject(data);
    } catch (error) {
        throw error;
    }
}

function _parseDateInObject(data) {
    for (const key in data) {
        if (!data.hasOwnProperty(key)) {
            continue;
        }
        data[key] = _parseDateInStringOrDateObject(data[key]);
    }

    return data;    
}

function _parseDateInArray(data) {
    if (!Array.isArray(data)) {
        return data;
    }

    for (const iterator of data) {
        _parseDateInObject(iterator);
    }
    
    return data;
}

function _parseDateInStringOrDateObject(data) {
    const date = moment(data, moment.ISO_8601, true);
    if (date.isValid()) {
        return date.locale('zh-cn').format('lll');
    }
    return data;    
}

module.exports = { parseDate };