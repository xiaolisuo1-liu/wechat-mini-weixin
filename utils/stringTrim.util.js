async function stringTrimPipe(data, length, attributes = []) {

    if (typeof data === 'string') {
        return _stringTrim(data, length);
    }

    if (Array.isArray(data)) {
        return stringTrimInArray(data, length, attributes);
    }

    if (typeof data === "object") {
        return stringTrimInObjcet(data, length, attributes);
    }


    return data;
}

function stringTrimInObjcet(data = {}, length, attributes = []) {
    for (const key in data) {
        
        let element = data[key];
        for (const attr of attributes) {
            if (key === attr) {
                data[key] = _stringTrim(element, length);
            }
        }
    }

    return data;
}

function stringTrimInArray(data = [], length, attributes = []) {
    for (let iterator of data) {
        iterator = stringTrimInObjcet(iterator, length, attributes);
    }

    return data;
}



function _stringTrim(str = '', length = 5) {

    if (str.length <= length) {
        return str;
    }

    return `${str.substr(0, length)}...`;
    
}

module.exports = { stringTrimPipe };