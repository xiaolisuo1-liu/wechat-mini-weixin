const app = new getApp();
const env = require('../env.js');

function ajax(url, data, method = 'POST', auth = null) {
  return new Promise((resolve, reject) => {
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: env.apiUrl + url,
      data: data,
      method: method,
      header: {
        'content-type': 'application/json', // 默认值
        'client-key': 'CH!NA_M!Ni_@PP',
        'x-auth': auth,
        'languageId': env.languageId,
        'platform':'users'
      },
      success(res) {
        resolve(res.data)
      },
      fail(err) {
        console.log(err)
        reject(err)
      },
      complete() {
        wx.hideLoading();
      }
    })
  })
}

function isset(value) {
  return value !== '' && value !== undefined && value !== null;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function toPromise(callback, args = {}) {
  return new Promise((resolve, reject) => {
    try {
      callback({
          ...args,
          success: (res) => resolve(res),
          fail: (err) => reject(err)
      });
    } catch (error) {
      reject(error);      
    }
  });
}

function showToast(title, type = 'success') {
  if (type == 'none') {
    return wx.showToast({
      title: title,
      icon: 'none'
    })
  } else if (type == 'success') {
    return wx.showToast({
      title: title
    })
  }
}

function base64_decode(input) { // 解码，配合decodeURIComponent使用
  var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var output = "";
  var chr1, chr2, chr3;
  var enc1, enc2, enc3, enc4;
  var i = 0;
  input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  while (i < input.length) {
    enc1 = base64EncodeChars.indexOf(input.charAt(i++));
    enc2 = base64EncodeChars.indexOf(input.charAt(i++));
    enc3 = base64EncodeChars.indexOf(input.charAt(i++));
    enc4 = base64EncodeChars.indexOf(input.charAt(i++));
    chr1 = (enc1 << 2) | (enc2 >> 4);
    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
    chr3 = ((enc3 & 3) << 6) | enc4;
    output = output + String.fromCharCode(chr1);
    if (enc3 != 64) {
      output = output + String.fromCharCode(chr2);
    }
    if (enc4 != 64) {
      output = output + String.fromCharCode(chr3);
    }
  }
  return utf8_decode(output);
}

// Initially, we're not waiting
var wait = false;
function throttle(callback, limit) {
  // We return a throttled function
  if (!wait) {
    // If we're not waiting
    callback.call(); // Execute users function
    wait = true; // Prevent future invocations
    setTimeout(function () {
      // After a period of time
      wait = false; // And allow future invocations
    }, limit);
  }
}

function codeStructureIsDefined(base64Str = '') {
 try {
    const decoded = base64_decode(base64Str);
    const binCode = JSON.parse(decoded).content.binCode;

    if (typeof binCode !== 'string') {
      return false;
    }

    return true;
 } catch (err) {
    return false;
 }
}

function isBase64(str = '') {
  return RegExp(/[A-Za-z0-9+/=]/).test(str) && str.endsWith('=');
}

function utf8_decode(utftext) { // utf-8解码
  var string = '';
  let i = 0;
  let c = 0;
  let c1 = 0;
  let c2 = 0;
  while (i < utftext.length) {
    c = utftext.charCodeAt(i);
    if (c < 128) {
      string += String.fromCharCode(c);
      i++;
    } else if ((c > 191) && (c < 224)) {
      c1 = utftext.charCodeAt(i + 1);
      string += String.fromCharCode(((c & 31) << 6) | (c1 & 63));
      i += 2;
    } else {
      c1 = utftext.charCodeAt(i + 1);
      c2 = utftext.charCodeAt(i + 2);
      string += String.fromCharCode(((c & 15) << 12) | ((c1 & 63) << 6) | (c2 & 63));
      i += 3;
    }
  }
  return string;
}
//腾讯转百度 
function qqMapTransBMap(lng, lat) { let x_pi = 3.14159265358979324 * 3000.0 / 180.0; let x = lng; let y = lat; let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi); let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi); let lngs = z * Math.cos(theta) + 0.0065; let lats = z * Math.sin(theta) + 0.006; return { lng: lngs, lat: lats }; }
//百度转腾讯
function BMapTransqqMap(lng, lat) { let x_pi = 3.14159265358979324 * 3000.0 / 180.0; let x = lng - 0.0065; let y = lat - 0.006; let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi); let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi); let lngs = z * Math.cos(theta); let lats = z * Math.sin(theta); return { lng: lngs, lat: lats }; }


function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

function toHexString(byteArray) {
  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('')
}

function buf2hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

function hex2buf(hex) {
  return new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
    return parseInt(h, 16)
  }))
}

function arrayBufferToString(buffer){
  var arr = new Uint8Array(buffer);
  var str = String.fromCharCode.apply(String, arr);
  if(/[\u0080-\uffff]/.test(str)){
      throw new Error("this string seems to contain (still encoded) multibytes");
  }
  return str;
}


module.exports = {
  ajax,
  showToast,
  base64_decode,
  codeStructureIsDefined,
  isset,
  toPromise,
  throttle,
  ab2str,
  str2ab,
  toHexString,
  buf2hex,
  hex2buf,
  arrayBufferToString,
  sleep,
}