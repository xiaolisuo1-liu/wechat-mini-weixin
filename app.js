App({
  onLaunch: function () {
    var value = wx.getStorageSync('userInfo');
    this.globalData.userInfo = value;

    if (!value.token){
      wx.reLaunch({
        url:'pages/landingPage/landingPage'
      })
    }
  },
  globalData: {
    userInfo: null,
    birds:null,
    user_profile:null
  }
})