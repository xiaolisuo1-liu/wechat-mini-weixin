const production = {
  apiUrl: 'https://api.aixunhuan.com.cn/api/v1.0/',
  webUrl: 'https://vms.aixunhuan.com.cn/',
  languageId: 3,
};

const development = {
  apiUrl: 'https://api-dev.aixunhuan.com.cn/api/v1.0/',
  webUrl: 'https://vms-dev.aixunhuan.com.cn/',
  languageId: 1,
};


function getCurrentEnv() {
  console.log("Can I use env? ", wx.canIUse('getAccountInfoSync'))
  if (!wx.canIUse('getAccountInfoSync')) {
    return 'production';
  }

  const { miniProgram } = wx.getAccountInfoSync();

  return miniProgram.envVersion;
}

function getEnvVars() {
  console.log(getCurrentEnv());
  const devEnvs = ['develop', 'trial'];
  const isEnvDev = devEnvs.findIndex(env => env === getCurrentEnv());

  if (isEnvDev !== -1) {
    return Object.assign(production, development);
  }

  return production;
}

const envVars = getEnvVars();
module.exports = envVars;